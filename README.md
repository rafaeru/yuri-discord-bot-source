# Yuri Discord bot

Este projeto foi abandonado à muito tempo e por isso provavelmente não será mais atualizado. Era suposto ser um bot para o [Discord](https://discordapp.com/) baseado na personagem [Yuri](https://ddlcwiki.net/wiki/Yuri).

Este projeto foi escrito em *JavaScript* para ser usado com [node.js](https://nodejs.org/en/download/).

## Transferir projeto

Para transferir este projeto podes usar o seguinte comando.
**Requer [git](https://git-scm.com/downloads) instalado.**

    git clone https://gitlab.com/rafaeru/yuri-discord-bot-source.git

### Executar bot

**Dependências:**

* [discord.js](https://www.npmjs.com/package/discord.js)

#### Token

No ficheiro [`/.env`](/.env) é preciso colocar o token do bot. Um token pode ser obtido [aqui](https://discordapp.com/developers/applications/).

Exemplo: `token=pqPFtMghaANRgErkHRLkkZJCfvjKZ`

#### Iniciar bot

É preciso o [node.js](https://nodejs.org/en/download/) instalado. O bot pode ser iniciado com o seguinte comando.

    node start bot.js

---

Este mini tutorial está super incompleto, se quiseres saber mais vai ver outras fontes bem documentadas.

---

> *Esta mensagem foi atualizada a: 31/10/2019*